import os.path
import sys


# Method will parse the text file, sort it, and return the
# different components
# Input: Name of text file passed through terminal
# Output:
#       col      - int representing number of columns
#       row      - int representing number of rows
#       charGrid - 2D String matrix of letters representing
#                  the search area
#       wordBank - A list of all the words we want to search                 for
def readfile(filename):
    # Reads the first line of text file and seperates rows and columns
    def getRowCol(firstLine):
        dimensions = firstLine.split("x", 1)
        return int(dimensions[0]), int(dimensions[1])

    # Parses the grid in the text file to be saved in a 2D matrix
    def createMatrix(matrixLines, row, col):
        charGrid = [[0 for _ in range(row)] for _ in range(col)]

        for x in range(col):
            charGrid[x] = matrixLines[x].split(" ")

        return charGrid

    if os.path.isfile(filename):
        f = open(filename, "r")
        firstLine = f.readline()
        col, row = getRowCol(firstLine)

        matrixLines = []
        for line in range(col):
            matrixLines.append(f.readline().replace("\n", ""))

        charGrid = createMatrix(matrixLines, row, col)

        wordBank = f.read().splitlines()

        return col, row, charGrid, wordBank

    else:
        sys.exit("File Could Not Be Found")


# Method that will preform the word search
# Inputs:
#         wordBank - list of words to search for
#         charGrid - 2D matrix of letters
# Output: A dictionary where the key is the found word and
# the value is the coordinate path representing where the
# word is found in the grid
def searchWords(wordBank, charGrid):
    rows = len(charGrid)
    cols = len(charGrid[0])

    # Method that checks if the row and col are within the bounds of the grid
    def in_bounds(row, col):
        return row >= 0 and row < rows and col >= 0 and col < cols

    # Method that performs a Depth First Search for searching on the diagnolas
    def dfs(row, col, index, path, visitedPaths):
        # If we've found the whole word, return True
        if index == len(word):
            return True, path[:-1]  # The path adds the very next coordinate to ensure the order
                                    # of the search is correct, so when we return we will just omit the extra
                                    # coordinate
                                    
        # If we're out of bounds or the current character doesn't match, return False
        if not in_bounds(row, col) or charGrid[row][col] != word[index]:
            return False, []
        # If we've already explored this path, return False
        if visitedPaths[row][col][index] != -1:
            return False, []
        # Mark the current position as visited
        visitedPaths[row][col][index] = 1
        # Recursively search the diagonals
        for deltaRow, deltaCol in [(1, 1), (-1, 1), (1, -1), (-1, -1)]:
            if (row + deltaRow, col + deltaCol) not in path:
                found, subpath = dfs(
                    row + deltaRow, col + deltaCol, index + 1, path + [(row + deltaRow, col + deltaCol)], visitedPaths
                )
                if found:
                    return True, subpath
        # Unmark the current position as visited
        visitedPaths[row][col][index] = 0
        # If we didn't find the word, return False
        return False, []

    # Search for all words in the grid
    results = {}
    for wordSpace in wordBank:
        word = wordSpace.replace(" ", "")
        visitedPaths = [
            [[-1 for _ in range(len(word) + 1)] for _ in range(cols)]
            for _ in range(rows)
        ]
        # Search horizontally
        for row_index, row in enumerate(charGrid):
            row_str = "".join(row)
            if word in row_str:
                start_col = row_str.index(word)
                end_col = start_col + len(word)
                results[wordSpace] = [
                    (row_index, col) for col in range(start_col, end_col)
                ]
            elif word[::-1] in row_str:
                start_col = row_str.index(word[::-1])
                end_col = start_col + len(word)
                results[wordSpace] = [
                                         (row_index, col) for col in range(start_col, end_col)
                                     ][::-1]
        # Search vertically
        for col_index in range(cols):
            col_str = "".join([row[col_index] for row in charGrid])
            if word in col_str:
                start_row = col_str.index(word)
                end_row = start_row + len(word)
                results[wordSpace] = [
                    (row, col_index) for row in range(start_row, end_row)
                ]
            elif word[::-1] in col_str:
                start_row = col_str.index(word[::-1])
                end_row = start_row + len(word)
                results[wordSpace] = [
                                         (row, col_index) for row in range(start_row, end_row)
                                     ][::-1]
        # Search diagonally
        for row in range(rows):
            for col in range(cols):
                found, path = dfs(row, col, 0, [(row, col)], visitedPaths)
                if found:
                    results[wordSpace] = path

    return results


# Method takes in a dictionary of the found word and the
# entire path and returns a list of strings that is in the
# required format
def formatResults(results):
    finalResults = []
    for res in results:
        final = "{word} {startRow}:{startCol} {endRow}:{endCol}".format(
            word=res,
            startRow=results[res][0][0],
            startCol=results[res][0][1],
            endRow=results[res][len(results[res]) - 1][0],
            endCol=results[res][len(results[res]) - 1][1],
        )
        finalResults.append(final)

    return finalResults


if __name__ == "__main__":
    filename = sys.argv[1]
    col, row, charGrid, wordBank = readfile(filename)
    results = searchWords(wordBank, charGrid)
    finalResults = formatResults(results)
    for final in finalResults:
        print(final)
